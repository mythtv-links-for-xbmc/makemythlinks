

This script creates XBMC friendly hardlinks to mythtv recordings using 
metadata from the mythtv database.  The hardlinks are named in a human 
readable format including a pattern that XBMC can use to identify  the 
recordings. If hardlinks have been created, a command is issued that 
tells xbmc to update its video library. If videos are deleted via mythtv, 
the hardlinks are removed and a command is issued that tells xbmc to 
clean its video library. curl must be installed for this to work.  
xbmc must be configured to allow it to be controled remotely.In its 
current form this script must be run on the machine that is running 
mysql server. The scipt can be run using mythtv events or via cron.   
Tested with mpg files but I guess it could work with other formats. I'm 
not sure about nupple files though.                                             
                                                                            
**VARIABLES

The following variables must be set within the script in order to run 
the script.

Most of the variables have default values assiged to them but can be 
changed if wanted.             
                                                                            
      tvDirName:folder where the script will create links for the tvshows   
   movieDirName:folder where links for the movies wil be created            
    linkDirName:parent folder of movieDirName and tvDirName                 
lockFileDirName:directory to keep lock file                                 
 logFileDirName:directory to keep log file                                  
      staleTime:seconds after which lock file is considered stale
   cleanLibTime:minutes to sleep after issuing the clean library command
    scanLibTime:minutes to wait after issuing the  scan library command           
   xbmcUserName:xbmc username                                               
   xbmcPassword:xbmc password                                               
  xbmcIpAddress:ip address of the machine that is running xbmc              
   xbmcPort the:port that xbmc is listening on

*******DEFAULT VALUES***********
tvDirName="tvShows" 
movieDirName="movies" 
linkDirName="links" 
lockFileDirName="lock" 
logFileDirName="log" 
staleTime="1800" 
cleanLibTime="6" 
scanLibTime="2" 
********************************



The mysql username and password are automatically extracted from the 
config file. 
  mySQLUserName:mysql database user name (automatically detected)           
  mySQLPassword:mysql database password  (automatically detected)                              
                                                                            
recordingsLocation is automatically retrieved from the myth database.       
The mythtv user should already have write access in this location.                   
"title" is created upon detection of a new tv series.                         
                                           
Hardlink location for tv shows will be created as follows                   
recordingsLocation/linkDirName/tvDirName/title/tvDBFileName                 
                                                                            
Hardlink location for movies will be created as follows                     
recordingsLocation/linkDirName/movieDirName/movieFileName                   
                                                                            


**CONFIGURING & RUNNING

This script does not do the actual scraping for XBMC but rather it
tells an instance of XBMC to scrape and clean.


To allow remote control of XBMC, start the XBMC instance that will do the 
scraping and cleaning. XBMC does not have to reside on the machine with 
the mysql server.  

To configure XBMC to be remotely controlled, 
navigate to                                

system->settings->services->webserver                                       
Enable "Allow control of XBMC via HTTP" and create a username and password.                            
Enable "Allow programs on other systems to control XBMC"                    
system->settings->services->Remote control 
     

You should only need to configure 4 variables within this script for it to 
work.

xbmcUserName="xbmc"           # xbmc username
xbmcPassword="xbmc"           # xbmc password
xbmcIpAddress="192.168.1.111" # ip address of the machine that is running xbmc
xbmcPort="8080"               # the port that xbmc is listening on


The script will attempt to automatically get the mythtv username and password
from "/etc/mythtv/config.xml" with the follwing grep expressions.

mySQLUserName=`grep -oP '(?<=<UserName>).*?(?=</UserName>)' /etc/mythtv/config.xml`
mySQLPassword=`grep -oP '(?<=<Password>).*?(?=</Password>)' /etc/mythtv/config.xml`

if the path and/or filename is different on your system then update it accordingly.
You can also replace the expressions with the mythtv username and password.

eg.
mySQLUserName="macleod"
mySQLPassword="h1land3r"

Although one can run the script as any user, the script really should be
run as the mythtv user. This ensures that links created are owned
by mythtv.

To run the script as "mythtv" login as user mythtv.             
Under ubuntu 12.04 one can issue the command "sudo su mythtv -" 
when prompted, provide your root password. 
Navigate to where the script is and type                     

"bash makeMythLinks"  

Lets say you have an episode of a tvshow and a movie in your mythtv recorings
and you've set mythtv to record in /media/recordings/mythtv.


Tv Show
-Title:Family Guy
-Subtitle:The Simpsons Guy
-Season:13
-Episode:1

Movie
-Title:The Lone Ranger
-Year:2013


the "recordingsLocation" variable will automatically be set to "/media/recordings/mythtv".
Assuming that you are using the default values for the location names,
the following links will be created.

"/media/recordings/mythtv/links/tvShows/family_guy/the_simpsons_guy.s13e1.mpg"
"/media/recordings/mythtv/links/movies/the_lone_ranger(2013).mpg"

In this example you need to make sure that "/media/recordings/mythtv/links" is 
shared on your network and that XBMC can see it.
You also need to set the correct content type for the movies directory and the
tvShows directory.

This script can be run as a mythtv event. For example you can set it to
run after a recording has ended.

This script can also be run as a cron job. 
maybe once an hour.















                                                      
                                                                            
